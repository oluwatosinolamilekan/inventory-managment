<!DOCTYPE html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <title>Inventory Management</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
      <link href="https://fonts.googleapis.com/css?family=Mada:300,400,500,600,700" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{asset('backend/bower_components/bootstrap/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/icon/themify-icons/themify-icons.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/icon/icofont/css/icofont.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/style.css')}}">
   </head>
   <body class="fix-menu">
      <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <div class="login-card card-block auth-body m-auto">
                     <form class="md-float-material">
                        <div class="text-center">
                           <img src="{{asset('backend/assets/images/logo.png')}}}" alt="logo.png">
                        </div>
                        <div class="auth-box">
                           <div class="row m-b-20">
                              <div class="col-md-12">
                                 <h3 class="text-left txt-primary">Sign In</h3>
                              </div>
                           </div>
                           <hr />
                           <div class="input-group">
                              <input type="email" class="form-control" placeholder="Your Email Address">
                              <span class="md-line"></span>
                           </div>
                           <div class="input-group">
                              <input type="password" class="form-control" placeholder="Password">
                              <span class="md-line"></span>
                           </div>
                           <div class="row m-t-25 text-left">
                              <div class="col-sm-7 col-xs-12">
                                 <div class="checkbox-fade fade-in-primary">
                                    <label>
                                    <input type="checkbox" value="">
                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                    <span class="text-inverse">Remember me</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-sm-5 col-xs-12 forgot-phone text-right">
                                 <a href="auth-reset-password.html" class="text-right f-w-600 text-inverse"> Forgot Your Password?</a>
                              </div>
                           </div>
                           <div class="row m-t-30">
                              <div class="col-md-12">
                                 <button type="button" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Sign in</button>
                              </div>
                           </div>
                           <hr />
                           <div class="row">
                              <div class="col-9">
                                 <p class="text-inverse text-left m-b-0">Thank you and enjoy our website.</p>
                                 <p class="text-inverse text-left"><b>Your Autentification Team</b></p>
                              </div>
                              <div class="col-3">
                                 <img src="assets/images/auth/Logo-small-bottom.png" alt="small-logo.png">
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script type="text/javascript" src="{{asset('backend/bower_components/jquery/js/jquery.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/popper.js/js/popper.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/modernizr/js/modernizr.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/modernizr/js/css-scrollbars.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/i18next/js/i18next.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('backend/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>
   </body>
</html>